#
# This file is part of application-utility.
#
# application-utility is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# application-utility is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with application-utility.  If not, see <http://www.gnu.org/licenses/>.
#
# Authors: papajoke
#          fhdk


import logging
import os
import sys

from application_utility.browser.config import Config
from application_utility.browser.exceptions import NoAppInIsoError

from application_utility.translation import i18n

_ = i18n.language.gettext


class HelloConfig(Config):
    """
    Created in Manjaro-Hello
    """

    def load(self):
        """ load data"""
        if os.path.isfile("/run/miso/bootmnt/manjaro"):
            raise NoAppInIsoError()
        else:
            if "--dev" in sys.argv:
                logging.basicConfig(level=logging.DEBUG)
            else:
                logging.basicConfig(level=logging.INFO)

        logging.debug(":AppUtil:HelloConfig:_APPS_DATA_DIR is %s", self.APPS_DATA_DIR)
        logging.debug(":AppUtil:HelloConfig:_APPS_PREF_FILE is %s", self.APPS_PREF_FILE)

        self.pref = self.read_json_file(self.APPS_PREF_FILE)
        logging.debug(":AppUtil:HelloConfig:self.pref is %s", self.pref)

        # TODO to set ?
        self.url = {
            "desktop": "",
            "main": ""
        }
        logging.debug(":AppUtil:HelloConfig:self.url is %s", self.url)

        self.file = {
            "desktop": "",
            "main": f"{self.APPS_DATA_DIR}/{self.pref['data-set']}.json"
        }
        logging.debug(":AppUtil:HelloConfig:self.file is %s", self.file)

        self.file["main"] = self.get_datafile(self.file["main"], "file")
        logging.debug(":AppUtil:HelloConfig:self.file is %s", self.file)

        return self
